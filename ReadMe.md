# Fit4U

## Project Description

This application is intended to allow members of a gym to experience new comfortable experience working out in this new age of the pandemic. This application allows users to reserve private work out rooms, ensuring a safe and comfortable experience. The application also provides support by including a knowledge base on workout and equipments

## Technologies Used

* Kotlin - version 30.0
* Android Studio - version 4.1.3
* Retrofit 
* Glide
* Coroutines
* MVVM
* Databinding Expressions
* Jetpack Navigation
* Jetpack Safe args

## Features

List of features:
* Login and Register Functionality
* Search through rooms available based on time, data, and equipment
* Make Reservation of given room
* Confirmation screen showing dynamic details such as data, time, ID.
* Save reservation to default Android OS calander with all reservation details
* List of equipment and information about it
* Different workouts for people to do

## Getting Started
   
Git Clone: "git@ gitlab .git@gitlab.com:a.m.p/beefcake.git"

Http Clone: https://gitlab.com/a.m.p/beefcake.git

Instructions for Windows:
* Download files
* Open files in Android Studio 4.1.3 or later
* Run Project



## Usage

This application uses our own created webservices, but still serves as a mock application due to properties not actually being listed.

## Contributors

* Rogerr Oliva
* Mathew Woods
* Adam Savoia
* Ankit Patel

## License

This project uses the following license: [<license_name>](<link>).
